const path = require("path")

function resolvePath(dir) {
    return path.join(__dirname, dir)
}

module.exports = {
    devServer: {
        port: 8088,
        open: true, // 自动打开
        // proxy: 'http://localhost:8081'
        host: 'localhost',
        stats: 'errors-only' // 打包日志输出错误信息
    },
    chainWebpack: config => {
        config.module
            .rule("markdown")
            .test(/\.md$/)
            .use("html-loader")
            .loader("html-loader")
            .end()
            .use("markdown-loader")
            .loader("markdown-loader")
            .end()


        // .rule("expose1")
        // .test(require.resolve("jquery"))
        // .use()
        // .loader("expose-loader")
        // .options("jQuery")
        // .end()

        /* config.module
          .rule("expose2")
          .test(require.resolve("jquery"))
          .use()
          .loader("expose-loader")
          .options("$")
          .end()
        config.module
          .rule("vue")
          .use("iview-loader") // 解决ivew组件 忽略前缀i的问题
          .loader("iview-loader")
          .options({
            prefix: false,
          })
          .end() */

        /* config.module
          .rule("scss")
          .oneOf("vue")
          .use("px2rem-loader")
          .loader("px2rem-loader")
          .before("postcss-loader") // this makes it work.
          .options({ remUnit: 75, remPrecision: 8 })
          .end() */
    },
    configureWebpack: {
        // rules:[{
        //   test: /\.md$/,
        //   use: [
        //     {
        //       loader: "html-loader",
        //     },
        //     {
        //       loader: "markdown-loader",
        //     },
        //   ],
        // }],
        devtool: "source-map",
        resolve: {
            // css 需要使用  ~ 波浪号前缀
            alias: {
                "@": resolvePath("src"),
                "@icon": resolvePath("src/assets/icon"),
                "@image": resolvePath("src/assets/im/"),
                "@css": resolvePath("src/assets/css/"),
                "@hjs": resolvePath("public/highlight"),
            },
            extensions: ['.js', '.vue', '.JSON', '.ts', '.css']
        },
    },
    // productionSourceMap: true,

    css: {
        sourceMap: true,
    },
}