import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

const store = new Vuex.Store({
  state() {
    return {
      tableSetting: [
        {
          value: "date",
          label: "日期",
        },
        {
          value: "name",
          label: "名字",
        },
        {
          value: "address",
          label: "地址",
        },
      ],
    };
  },
  mutations: {
    updateTableSetting(state, payload) {
      state.tableSetting = payload;
    },
  },
});

export default store;
