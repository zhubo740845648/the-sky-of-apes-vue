import ZbInput from './ZbInput'
import ZbSelect from './ZbSelect'

const fields = [{
        vmodel: '',
        enable: true, // 可切换的动态值
        disabled: false, // 可切换的动态值
        component: ZbInput,
    },
    {
        vmodel: '',
        enable: true, // 可切换的动态值
        disabled: false, // 可切换的动态值
        options: [{ label: '张三', value: 1 }, { label: '李四', value: 2 }],
        component: ZbSelect,
    }
]

const variableFormat = vm => {
    return fields.map(e => {
        e.enable = vm.enableCom
        e.disabled = vm.disabledCom
        return e
    })
}
export default variableFormat