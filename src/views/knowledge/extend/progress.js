// import api from "@/api/mock"
import Vue from "vue"
import ProgressBar from "./ProgressBar.vue"

const Progress = Vue.extend(ProgressBar)
Vue.prototype.$Progress = async (api, params) => {
  let resolveHandle, rejectHandle
  let successHandle = res => resolveHandle(res)
  let errorHandle = err => rejectHandle(err)
  new Progress({
    propsData: { api, successHandle, errorHandle, params },
  }).$mount()

  return new Promise((resolve, reject) => {
    resolveHandle = resolve
    rejectHandle = reject
  })
}
