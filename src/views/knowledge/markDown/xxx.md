这是一个md

```vue
<template>
  <div class="mark-down">
    <mavon-editor
      class="md"
      v-html="handbook"
      :subfield="false"
      :toolbarsFlag="false"
      :boxShadow="false"
      :ishljs="true"
    />
  </div>
</template>

<script>
console.log("markDown加载...")
// 引入makrdown插件
import mavonEditor from "mavon-editor"
import "mavon-editor/dist/css/index.css"
import Vue from "vue"
Vue.use(mavonEditor)
import GUID from "./xxx.md"

export default {
  data() {
    return {
      markdownOption: {
        bold: true, // 粗体
      },
      handbook: GUID,
    }
  },
  created() {},
}
</script>

<style lang="less" scoped>
.mark-down {
  font-size: 16px;
}
</style>

```



