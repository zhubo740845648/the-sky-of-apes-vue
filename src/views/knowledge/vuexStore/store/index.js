const tableStore = {
  namespaced: true,
  state() {
    return {
      tableSetting: [
        // {
        //   value: "date",
        //   label: "日期",
        // },
        // {
        //   value: "name",
        //   label: "名字",
        // },
        // {
        //   value: "address",
        //   label: "地址",
        // },
      ],
      storeName: "tableStore",
    };
  },
  mutations: {
    updateTableSetting(state, payload) {
      state.tableSetting = payload;
      console.log('tableStore模块')
    },
  },
};
export default tableStore;
