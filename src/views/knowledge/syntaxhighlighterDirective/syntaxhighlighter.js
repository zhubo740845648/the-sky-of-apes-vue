const publicPath = process.env.BASE_URL
const preCodeJs = ['XRegExp.js', 'shCore.js', 'shBrushJScript.js']
export async function syntaxhighlighter() {
    let tag = document.getElementsByTagName('script')
    tag = [].slice.call(tag)
    for (let scriptHtmlReady of preCodeJs) {
        // tag.forEach(e => {
        //     console.log(e.src)
        // })
        // console.log(`${location.origin}/scripts/XRegExp.js`)
        if (tag.some(i => i.src === `${location.origin}/syntaxhighlighter/scripts/XRegExp.js`)) {
            console.log('===')
            continue
        }


        await new Promise((resolve, reject) => {
            const script = document.createElement('script')
            script.type = 'text/javascript'
            script.src = `${location.origin}${publicPath}syntaxhighlighter/scripts/${scriptHtmlReady}`
            script.onerror = reject
            document.body.appendChild(script)
            script.onload = () => {
                console.log('1')
                resolve('成功')
            }
        })
    }
}