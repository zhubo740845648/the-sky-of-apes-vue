export default [{
        name: 'Auto Rename Tag',
        explain: '同步修改收尾标签'
    },
    {
        name: 'chinese',
        explain: '中文'
    },
    {
        name: 'easy less',
        explain: '保存less文件时自动生成css文件'
    },
    {
        name: 'code runner',
        explain: '右键添加运行js快捷指令'
    },
]