export default [{
        name: 'Auto Rename Tag',
        explain: `
        把模板编译为render函数
        实例进行挂载, 根据根节点render函数的调用，递归的生成虚拟dom
        对比虚拟dom，渲染到真实dom
        组件内部data发生变化，组件和子组件引用data作为props重新调用render函数，生成虚拟dom, 返回到步骤3
        `
    },
    {
        name: 'chinese',
        explain: '中文'
    },
    {
        name: 'easy less',
        explain: '保存less文件时自动生成css文件'
    },
    {
        name: 'code runner',
        explain: '右键添加运行js快捷指令'
    },
]