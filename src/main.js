import Vue from "vue"
import App from "./App.vue"
import { router } from "./router"
import store from "./store"

import ElementUI from "element-ui"
import "element-ui/lib/theme-chalk/index.css"
Vue.use(ElementUI)

import "./css/index.css";
Vue.config.productionTip = false; // 去除打印台提示


import hljs from 'highlight.js' //导入代码高亮文件
import 'highlight.js/styles/monokai-sublime.css' //导入代码高亮样式
//自定义一个代码高亮指令
Vue.directive('highlight', function(el) {
    const blocks = el.querySelectorAll('pre code');
    blocks.forEach((block) => {
        hljs.highlightBlock(block)
    })
})

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount("#app");
import "./css/index.css"