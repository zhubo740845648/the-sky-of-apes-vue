"use strict";
// 类型注解
(function () { return function showMsg(str) {
    return '窗前明月' + str;
}; });
function showFullName(person) {
    return person.lastName + '-' + person.lastName;
}
var person = {
    firstName: '东方',
    lastName: '不败'
};
console.log(showFullName(person));
// class 类
var User = /** @class */ (function () {
    function User(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = firstName + '-' + lastName;
    }
    return User;
}());
function greeter(person) {
    return person.firstName + person.lastName + '你好';
}
var someOne = new User('诸葛', '孔明');
console.log(greeter(someOne));
