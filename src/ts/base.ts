// 定义枚举
const enum StateEnum {
  TO_BE_DONE = 0,
  DOING = 1,
  DONE = 2
}

// 定义 item 接口
interface SrvItem {
  val: string,
  key: string
}

// 定义服务接口
interface SrvType {
  name: string,
  key: string,
  state?: StateEnum,
  item: Array<SrvItem>
}

// 然后定义初始值（如果不按照类型来，报错肯定是避免不了的）
const types: SrvType = {
  name: '',
  key: '',
  item: []
}