// 类型注解

(() => function showMsg(str: string) {
    return '窗前明月' + str
})

// 接口类型

interface Iperson {
    firstName: string;
    lastName: string;
}
function showFullName(person: Iperson) {
    return person.lastName + '-' + person.lastName
}

const person = {
    firstName: '东方',
    lastName: '不败'
}
console.log(showFullName(person));

// class 类

class User {
    firstName: string
    lastName: string
    fullName: string

    constructor(firstName: string, lastName: string) {
        this.firstName = firstName
        this.lastName = lastName
        this.fullName = firstName + '-' + lastName
    }

}
function greeter(person: Iperson) {
    return person.firstName + person.lastName + '你好'
}

const someOne = new User('诸葛', '孔明')
console.log(greeter(someOne))


