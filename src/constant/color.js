const LAYOUT = {
    TOP: '#BDBDD0',
    LEFT: '#6D6D7D',
    CONTENT: '#C4C2C2',
}

export {
    LAYOUT
}