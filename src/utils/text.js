// 读取text文件

const http = require('http');


const hostname = '127.0.0.1';
const port = 3000;
var fs = require("fs");
var data = fs.readFileSync('input.txt');
const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html;charset=UTF-8');




    res.end(data.toString());

});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
})

function jsReadFiles(files) {
    if (files.length) {
        var file = files[0];
        var reader = new FileReader(); //new一个FileReader实例
        if (/text+/.test(file.type)) { //判断文件类型，是不是text类型
            reader.onload = function() {
                $('body').append('<pre>' + this.result + '</pre>');
            }
            reader.readAsText(file);
        } else if (/image+/.test(file.type)) { //判断文件是不是imgage类型
            reader.onload = function() {
                $('body').append('<img src="' + this.result + '"/>');
            }
            reader.readAsDataURL(file);
        }
    }
}


function load(name) {
    let xhr = new XMLHttpRequest(),
        okStatus = document.location.protocol === "file:" ? 0 : 200;
    xhr.open('GET', name, false);
    xhr.overrideMimeType("text/html;charset=utf-8"); //默认为utf-8
    xhr.send(null);
    return xhr.status === okStatus ? xhr.responseText : null;
}

let text = load("文件名.txt");

console.log(text); //输出到浏览器控制器中

//document.write(text); //打印在网页中

//document.write("<pre>"+text+"<pre/>"); //解决txt的换行无法打印到网页上的问题