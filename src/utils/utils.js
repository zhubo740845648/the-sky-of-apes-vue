/**
 *
 * @param {
 *   {one:{a:1,b:2},two:{c:3,d:4},three:{e:5,f:6}}
 * } target
 * @returns {
 *    {one:{a:1,b:2,c:3,d:4,e:5,f:6,}}
 * }
 */

export default function(target) {
  for (let i = 1, j = arguments.length; i < j; i++) {
    let source = arguments[i] || {}
    for (let prop in source) {
      // eslint-disable-next-line no-prototype-builtins
      if (source.hasOwnProperty(prop)) {
        let value = source[prop]
        if (value !== undefined) {
          target[prop] = value
        }
      }
    }
  }

  return target
}
