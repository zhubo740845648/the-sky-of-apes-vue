class Hunters {
    list = []
    name = '默认姓名'
    constructor(name) {
        this.name = name
    }
    publish(money) {
        console.log(this.name + '发布了任务====')
        this.list.forEach(fn => fn(money))
    }
    subscribe(target, fn) {
        console.log(this.name + '订阅了' + target.name)
        target.list.push(fn)
    }
}

const 张三 = new Hunters('张三')
const 李四 = new Hunters('李四')
const 王五 = new Hunters('王五')

李四.subscribe(张三, (money) => {
    console.log('李四关注了张三', '张三愿意付钱', money);

})
王五.subscribe(张三, (money) => {
    console.log('王五关注了张三', '张三愿意付钱', money);
})

张三.publish(200)