const allFn = (newVal, oldVal) => {
    console.log('打印', newVal, oldVal);
}

function defineReactive(data, key, val) {
    let dep = []
    Object.defineProperty(data, key, {
        enumerable: true,
        configurable: true,
        get() {
            dep.push(allFn)
            return val
        },
        set(newVal) {
            if (val == newVal) {
                return
            }
            for (let i = 0; i < dep.length; i++) {
                dep[i](newVal, val)
            }
            val = newVal
        }
    })
}
let obj = { name: '张三' }
defineReactive(obj, 'name', '李四')

const fn1 = function() {
    console.log(1)
}
const fn2 = function() {
    console.log(2)
}
const fn3 = function() {
    console.log(1)
}
let arr = [fn1, fn2, fn3]