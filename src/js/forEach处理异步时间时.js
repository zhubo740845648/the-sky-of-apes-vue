let fun = item => new Promise(function(resolve) {
    console.log("promise1", item)
    resolve(item)
})
let arr = [1, 2, 3]
let resArr = []
arr.forEach(async e => {
    console.log('e', e)
    let res = await fun(e)
    resArr.push(res)
    console.log('res', res)
})
console.log("resArr", resArr)



// const fun = item => new Promise(function(resolve) {
//     console.log("promise1", item)
//     resolve(item)
// })
// let arr = [1, 2, 3]
// let resArr = []
// async function fn() {
//     for (item of arr) {
//         console.log(item)
//         let res = await fun(item)
//         resArr.push(res)
//     }
//     console.log("resArr", resArr)
// }
// fn()
// console.log("resArr2", resArr)