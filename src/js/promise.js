const RESOLVE = "resolved"
// const REJECT = 'rejected'
const PENDING = "pending"

class ZBPromise {
  status = PENDING
  result = undefined
  onResolvedArr = []
  constructor(excution) {
    console.log("constructor start")
    const resolve = result => {
      console.log("resolve start")
      if (this.status === PENDING) {
        this.result = result
        this.status = RESOLVE
        this.onResolvedArr.map(fn => fn())
      }
    }
    excution(resolve)
    console.log("constructor end", excution)
  }
  then(onResolved) {
    console.log("then start")

    if (this.status === RESOLVE) {
      setTimeout(() => {
        onResolved(this.result)
      }, 0)
    }
    if (this.status === PENDING) {
      this.onResolvedArr.push(() => onResolved(this.result))
    }
  }
}

module.exports = ZBPromise
