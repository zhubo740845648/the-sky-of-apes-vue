import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);

import NotFoundComponent from "@/components/404.vue";

import father from "@/views/test/father";
import son from "@/views/test/son";

const routes = [
  {
    path: "/father",
    component: father,
    children: [{ path: "son", component: son }],
  },
];

const modules = [
  // /\.\/[^\/]+\/index.vue/ 匹配 ./[除了/的所有字符]/index.vue
  // /\.\/\w+\/index.vue/匹配 ./匹配单词字符/index.vue
  // 匹配views/knowledge目录下的一级index
  require.context("../views/knowledge", true, /\.\/\w+\/index.vue/),
  require.context("../views/styleCss", true, /\.\/\w+\/index.vue/),
];
modules.forEach((el) => {
  el.keys().forEach((filename) => {
    // console.log('filename',filename)
    // eg: ./login/index.vue
    // 获取组件配置
    const componentConfig = el(filename);
    // 取出.和/index.vue => /promise 用于路由
    const componentName = filename
      .replace(/^\./, "")
      .replace(/\/index.vue/, "");
    const component = Vue.component(
      componentName.replace(/\//, ""),
      componentConfig.default || componentConfig
    );
    routes.push({
      path: componentName,
      component: component,
    });
  });
});

const router = new VueRouter({
  mode: "history",
  routes: [...routes, { path: "*", component: NotFoundComponent }],
});

export { router, routes };
